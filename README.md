
# Networking 101: Organisation de votre infrastructure réseau
>  ```bash
> gcloud auth list
> ```
> La commande gcloud auth list affiche la liste des comptes Google Cloud authentifiés sur la machine locale, indiquant quel compte est actuellement actif pour les opérations gcloud.

> ```bash
> gcloud config list project
> ```
> La commande gcloud config list project affiche le projet Google Cloud actuellement configuré et utilisé par les commandes gcloud.

> ```bash
> gcloud config set compute/zone "europe-west4-b"
> export ZONE=$(gcloud config get compute/zone)
> ```
> La commande `gcloud config set compute/zone "europe-west4-b"` configure la zone de calcul par défaut à "europe-west4-b" pour les opérations `gcloud` futures, et `export ZONE=$(gcloud config get compute/zone)` assigne la zone configurée à une variable d'environnement `ZONE` sur le shell actuel.

> ```bash
> gcloud config set compute/region "europe-west4"
> export REGION=$(gcloud config get compute/region)
> ```
> La commande `gcloud config set compute/region "europe-west4"` définit la région de calcul par défaut à "europe-west4" pour les futures opérations `gcloud`, et `export REGION=$(gcloud config get compute/region)` stocke la région configurée dans une variable d'environnement `REGION` sur le shell actuel.

> ```bash 
>gcloud compute networks create taw-custom-network  --subnet-mode custom
> ```
> La commande `gcloud compute networks create taw-custom-network --subnet-mode custom` crée un nouveau réseau VPC personnalisé nommé "taw-custom-network" dans Google Cloud avec un mode de sous-réseau configuré sur "custom", permettant la création manuelle de sous-réseaux avec des plages d'adresses IP spécifiques.

> ```bash
> gcloud compute networks subnets create >subnet-europe-west4 \
>   --network taw-custom-network \
>   --region europe-west4 \
>   --range 10.0.0.0/16
>   ```
> La commande crée un sous-réseau nommé "subnet-europe-west4" dans le réseau VPC "taw-custom-network", spécifié pour la région "europe-west4", avec une plage d'adresses IP de "10.0.0.0/16".

> ```bash
> gcloud compute networks subnets create subnet-us-east1 \
>   --network taw-custom-network \
>   --region us-east1 \
>   --range 10.1.0.0/16
>   ```
> Cette commande crée un sous-réseau nommé "subnet-us-east1" dans le réseau VPC "taw-custom-network", localisé dans la région "us-east1", avec une plage d'adresses IP de "10.1.0.0/16".

> ```bash
> gcloud compute networks subnets create subnet-us-west1 \
>   --network taw-custom-network \
>   --region us-west1 \
>   --range 10.2.0.0/16
>   ```
> Cette commande crée un sous-réseau nommé "subnet-us-west1" dans le réseau VPC "taw-custom-network", situé dans la région "us-west1", avec une plage d'adresses IP de "10.2.0.0/16".

> ```bash
> gcloud compute networks subnets list \
>   --network taw-custom-network
>   ```
> Cette commande liste tous les sous-réseaux créés au sein du réseau VPC "taw-custom-network", fournissant des détails tels que leurs noms, régions, et plages d'adresses IP.

> ```bash
> gcloud compute firewall-rules create nw101-allow-http \
> --allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
> --target-tags http
> ```
> Cette commande crée une règle de pare-feu nommée "nw101-allow-http" dans le réseau VPC "taw-custom-network", autorisant le trafic TCP sur le port 80 (HTTP) depuis toutes les adresses IP (0.0.0.0/0) vers les instances marquées avec le tag "http".

> ```bash
> gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
> ```
> Cette commande crée une règle de pare-feu nommée "nw101-allow-icmp" dans le réseau VPC "taw-custom-network", autorisant le trafic ICMP (utilisé pour le ping et d'autres diagnostics réseau) vers les instances marquées avec le tag "rules".

> ```bash
> gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
> ```
> Cette commande crée une règle de pare-feu nommée "nw101-allow-internal" dans le réseau VPC "taw-custom-network", autorisant tout le trafic TCP (sur tous les ports de 0 à 65535), UDP (sur tous les ports de 0 à 65535), et ICMP au sein des plages d'adresses IP spécifiées ("10.0.0.0/16", "10.2.0.0/16", "10.1.0.0/16"), facilitant ainsi la communication interne entre les instances situées dans ces sous-réseaux.

> ```bash
> gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"
> ```
> Cette commande crée une règle de pare-feu nommée "nw101-allow-ssh" dans le réseau VPC "taw-custom-network", autorisant le trafic TCP sur le port 22 (utilisé pour SSH) vers les instances marquées avec le tag "ssh", facilitant l'accès sécurisé aux instances via SSH.

> ```bash
> gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"
> ```
> Cette commande crée une règle de pare-feu nommée "nw101-allow-rdp" dans le réseau VPC "taw-custom-network", autorisant le trafic TCP sur le port 3389, qui est utilisé pour le protocole de bureau à distance (Remote Desktop Protocol - RDP), permettant ainsi les connexions RDP aux instances du réseau.

> ```bash
> gcloud compute instances create us-test-01 \
> --subnet subnet-europe-west4 \
> --zone europe-west4-b \
> --machine-type e2-standard-2 \
> --tags ssh,http,rules
> ```
> Cette commande crée une instance de machine virtuelle (VM) nommée "us-test-01" dans la zone "europe-west4-b", avec le type de machine "e2-standard-2". Elle est connectée au sous-réseau "subnet-europe-west4" et possède des tags "ssh", "http", et "rules", ce qui permet d'appliquer des règles de pare-feu correspondantes pour autoriser le trafic SSH, HTTP, et d'autres règles spécifiées par "rules".

> ```bash
> gcloud compute instances create us-test-02 \
> --subnet subnet-us-east1 \
> --zone us-east1-d \
> --machine-type e2-standard-2 \
> --tags ssh,http,rules
> ```
> Cette commande crée une instance de machine virtuelle nommée "us-test-02" dans la zone "us-east1-d" avec le type de machine "e2-standard-2". Elle est connectée au sous-réseau "subnet-us-east1" et possède des tags "ssh", "http", et "rules", ce qui permet d'appliquer des règles de pare-feu correspondantes pour autoriser le trafic SSH, HTTP, et d'autres règles spécifiées par "rules".

> ```bash
> gcloud compute instances create us-test-03 \
> --subnet subnet-us-west1 \
> --zone us-west1-c \
> --machine-type e2-standard-2 \
> --tags ssh,http,rules
> ```
> Cette commande crée une instance de machine virtuelle nommée "us-test-03" dans la zone "us-west1-c" avec le type de machine "e2-standard-2". Elle est connectée au sous-réseau "subnet-us-west1" et possède des tags "ssh", "http", et "rules", ce qui permet d'appliquer des règles de pare-feu correspondantes pour autoriser le trafic SSH, HTTP, et d'autres règles spécifiées par "rules".

> ```bash
> ping -c 3 IP EXTERNE (par exemple ici : 34.138.108.138)
> ```
> Cette commande ping envoie trois paquets ICMP à l'adresse IP externe de l'instance de machine virtuelle "us-test-02". 

> ```bash
> ping -c 3 IP EXTERNE
> ```
> Cette commande ping envoie trois paquets ICMP à l'adresse IP externe de l'instance de machine virtuelle "us-test-03". 

> ```bash
> ping -c 3 us-test-02.us-east1-d
> ```
> Cette commande ping est destinée à l'adresse "us-test-02.us-east1-d", qui semble être une tentative d'adressage utilisant un format incorrect pour une adresse IP ou un nom de domaine. Pour exécuter la commande ping avec succès, vous devez spécifier l'adresse IP ou le nom d'hôte correct de l'instance cible dans la région us-east1-d.


> ```bash
> sudo apt-get update
> ```
> ```bash
> sudo apt-get -y install traceroute mtr tcpdump iperf > whois host dnsutils siege
> ```
> ```bash
> traceroute www.icann.org
> ```
> ```bash
> sudo apt-get update
> ```
> Cette commande met à jour la liste des paquets disponibles et leurs versions, mais ne met pas à jour les paquets eux-mêmes. Elle permet de synchroniser la liste des logiciels disponibles dans les référentiels configurés sur votre système avec les informations les plus récentes sur ces logiciels.

> ```bash
> sudo apt-get -y install traceroute mtr tcpdump iperf > whois host dnsutils siege
> ```
> Cette commande installe plusieurs outils utiles pour le diagnostic réseau et les tests de performance, notamment : traceroute, mtr (My traceroute), tcpdump (pour capturer et analyser le trafic réseau), iperf (pour mesurer les performances du réseau), whois (pour obtenir des informations sur les noms de domaine et les adresses IP), host (pour résoudre les noms d'hôtes en adresses IP et vice versa), dnsutils (qui contient des outils de résolution de noms DNS) et siege (un outil de benchmarking et de test de charge HTTP). L'option `-y` est utilisée pour confirmer automatiquement toutes les questions posées par le gestionnaire de paquets, ce qui est utile lors de l'installation de plusieurs paquets en une seule fois.

> ```bash
> iperf -s #run in server mode
> ```
> Cette commande lance iperf en mode serveur, ce qui signifie qu'elle écoute les connexions entrantes et effectue des mesures de performances réseau lorsque des clients se connectent à elle.

> ```bash
> iperf -c us-test-01.europe-west4-b #run in client mode
> ```
> Cette commande lance iperf en mode client et établit une connexion avec le serveur spécifié à l'adresse "us-test-01.europe-west4-b". Elle sera utilisée pour mesurer les performances du réseau entre le client (cette machine) et le serveur.

> ```bash
> gcloud compute instances create us-test-04 \
> --subnet subnet-europe-west4 \
> --zone europe-west4-c \
> --tags ssh,http
> ```
> Cette commande crée une nouvelle instance de calcul nommée "us-test-04" dans la zone "europe-west4-c" et la place dans le sous-réseau "subnet-europe-west4". L'instance sera marquée avec les balises "ssh" et "http", ce qui permettra de contrôler les règles de pare-feu et d'autres configurations réseau.

> ```bash
> sudo apt-get update
> ```
> Cette commande met à jour la liste des packages disponibles dans les dépôts configurés sur le système en utilisant le gestionnaire de packages `apt`.

> ```bash
> sudo apt-get -y install traceroute mtr tcpdump iperf > whois host dnsutils siege
> ```
> Cette commande installe plusieurs utilitaires réseau utiles sur le système. Voici ce que chaque utilitaire fait :
> - `traceroute`: Utilisé pour suivre le chemin pris par les paquets IP d'une source vers une destination, en affichant tous les nœuds (routeurs) traversés.
> - `mtr`: Combinaison de `ping` et `traceroute` qui fournit des informations détaillées sur le chemin pris par les paquets IP.
> - `tcpdump`: Outil de capture et d'analyse de paquets TCP/IP sur un réseau. Il peut être utilisé pour examiner le trafic réseau en temps réel.
> - `iperf`: Outil de mesure de la bande passante du réseau permettant de tester les performances de la connexion réseau en envoyant des données entre deux hôtes.
> - `whois`: Utilisé pour interroger une base de données WHOIS pour obtenir des informations sur les enregistrements de noms de domaine.
> - `host`: Utilitaire DNS pour effectuer des requêtes DNS telles que la résolution de noms d'hôtes en adresses IP et vice versa.
> - `dnsutils`: Ensemble d'outils DNS incluant `dig` et `nslookup`, utiles pour diagnostiquer et résoudre les problèmes DNS.
> - `siege`: Outil de benchmarking HTTP et HTTPS qui simule le trafic sur un serveur web pour tester ses performances.

> ```bash
> iperf -s -u #iperf server side
> ```
> Cette commande exécute `iperf` en mode serveur pour mesurer les performances du réseau en utilisant le protocole UDP. Voici ce que chaque option fait :
> - `-s`: Indique à `iperf` de fonctionner en mode serveur, attendant les connexions des clients.
> - `-u`: Indique à `iperf` d'utiliser le protocole UDP pour les tests de performance. Cela signifie que les mesures ne seront pas fiables car UDP ne garantit pas la livraison des paquets, mais cela peut être utile pour simuler des conditions de trafic plus réalistes sur le réseau.

> ```bash
> iperf -c us-test-02.us-east1-d -u -b 2G #iperf client side - send 2 Gbits/s
> ```
> Cette commande exécute `iperf` en mode client pour mesurer les performances du réseau en envoyant un trafic de 2 Gbits/s vers l'hôte nommé "us-test-02.us-east1-d" en utilisant le protocole UDP. Voici ce que chaque option fait :
> - `-c us-test-02.us-east1-d`: Spécifie l'hôte distant vers lequel `iperf` enverra le trafic.
> - `-u`: Indique à `iperf` d'utiliser le protocole UDP pour les tests de performance.
> - `-b 2G`: Définit la bande passante souhaitée pour l'envoi de données, dans ce cas-ci à 2 Gbits/s. Cela simule un trafic réseau à cette vitesse.

> ```bash
> iperf -s
> ```
> Cette commande exécute `iperf` en mode serveur, ce qui signifie qu'elle met en attente le serveur pour accepter les connexions des clients `iperf` afin de mesurer les performances du réseau.

> ```bash
> iperf -c us-test-01.europe-west4-b -P 20
> ```


# Fondamentales sur Terraform


```bash
gcloud auth list
```

La commande ci-dessus, `gcloud auth list`, affiche la liste des comptes Google Cloud authentifiés sur la machine locale. Elle permet de voir quels comptes sont actuellement connectés et actifs pour effectuer des opérations avec gcloud.

```bash
gcloud config list project
```
Résultat de la commande `terraform`
```bash
Usage: terraform [--version] [--help]  [args]

The available commands for execution are listed below. The most common, useful commands are shown first, followed by less common or more advanced commands. If you're just getting started with Terraform, stick with the common commands. For the other commands, please read the help and docs before usage.

Common commands: apply Builds or changes infrastructure console Interactive console for Terraform interpolations destroy Destroy Terraform-managed infrastructure env Workspace management fmt Rewrites config files to canonical format get Download and install modules for the configuration graph Create a visual graph of Terraform resources import Import existing infrastructure into Terraform init Initialize a Terraform working directory output Read an output from a state file plan Generate and show an execution plan providers Prints a tree of the providers used in the configuration push Upload this Terraform module to Atlas to run refresh Update local state file against real resources show Inspect Terraform state or plan taint Manually mark a resource for recreation untaint Manually unmark a resource as tainted validate Validates the Terraform files version Prints the Terraform version workspace Workspace management

All other commands: debug Debug output management (experimental) force-unlock Manually unlock the terraform state state Advanced state management 
```

La commande ci-dessus, `gcloud config list project`, affiche le projet Google Cloud actuellement configuré et utilisé par les commandes gcloud. Cela permet de vérifier rapidement quel projet est défini comme projet par défaut pour les opérations gcloud.

```bash
touch instance.tf
``` 
La commande `touch instance.tf` en bash crée un nouveau fichier vide nommé "instance.tf".

Ensuite aller dans l'editeur de texte de google et ajouter le code ci-dessous dans instance.tf
```bash
resource "google_compute_instance" "terraform" {
  project      = "qwiklabs-gcp-01-aefd70bc4ef6"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}
```

# terraform init

La commande `terraform init` initialise un nouveau répertoire de travail Terraform. Elle configure le répertoire pour utiliser les plugins et modules nécessaires à la gestion de l'infrastructure décrite dans les fichiers de configuration Terraform.

Voici le résultat :
```bash
Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/google...
- Installing hashicorp/google v5.15.0...
- Installed hashicorp/google v5.15.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

# terraform plan

La commande `terraform plan` est utilisée pour créer un plan d'exécution Terraform. Elle analyse les fichiers de configuration Terraform dans le répertoire de travail et affiche un résumé des actions que Terraform exécutera lors de l'application de ces fichiers pour mettre en œuvre l'infrastructure décrite. Cette commande permet de prévisualiser les modifications potentielles avant de les appliquer réellement.

Voici le résultat :
```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "qwiklabs-gcp-01-aefd70bc4ef6"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
              + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

```

# terraform apply

La commande `terraform apply` est utilisée pour appliquer les changements décrits dans les fichiers de configuration Terraform. Elle crée, met à jour ou supprime les ressources selon les modifications apportées au code Terraform depuis la dernière exécution. Avant d'appliquer les modifications, Terraform affiche un résumé des actions qu'il va entreprendre et demande une confirmation avant de procéder.

Voici le résultat :
```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "qwiklabs-gcp-01-aefd70bc4ef6"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
      + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

google_compute_instance.terraform: Creating...
google_compute_instance.terraform: Still creating... [10s elapsed]
google_compute_instance.terraform: Creation complete after 18s [id=projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

```


# terraform show

La commande `terraform show` affiche l'état actuel de l'infrastructure gérée par Terraform. Elle affiche les ressources créées ainsi que leurs attributs et valeurs actuelles. Cette commande est utile pour vérifier l'état actuel de l'infrastructure après l'application des modifications.

Voici le résultat :
```bash
# google_compute_instance.terraform:
resource "google_compute_instance" "terraform" {
    can_ip_forward       = false
    cpu_platform         = "Intel Broadwell"
    current_status       = "RUNNING"
    deletion_protection  = false
    effective_labels     = {}
    enable_display       = false
    guest_accelerator    = []
    id                   = "projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform"
    instance_id          = "3631703849039533754"
    label_fingerprint    = "42WmSpB8rSM="
    machine_type         = "e2-medium"
    metadata_fingerprint = "WRRcWNyHo0s="
    name                 = "terraform"
    project              = "qwiklabs-gcp-01-aefd70bc4ef6"
    self_link            = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform"
    tags_fingerprint     = "42WmSpB8rSM="
    terraform_labels     = {}
    zone                 = "us-east1-b"

    boot_disk {
        auto_delete = true
        device_name = "persistent-disk-0"
        mode        = "READ_WRITE"
        source      = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/disks/terraform"

        initialize_params {
            enable_confidential_compute = false
            image                       = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-11-bullseye-v20240110"
            labels                      = {}
            provisioned_iops            = 0
            provisioned_throughput      = 0
            size                        = 10
            type                        = "pd-standard"
        }
    }

    network_interface {
        internal_ipv6_prefix_length = 0
        name                        = "nic0"
        network                     = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/global/networks/default"
        network_ip                  = "10.142.0.2"
        queue_count                 = 0
        stack_type                  = "IPV4_ONLY"
        subnetwork                  = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/regions/us-east1/subnetworks/default"
        subnetwork_project          = "qwiklabs-gcp-01-aefd70bc4ef6"

        access_config {
            nat_ip       = "34.138.108.138"
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
        provisioning_model  = "STANDARD"
    }

    shielded_instance_config {
        enable_integrity_monitoring = true
        enable_secure_boot          = false
        enable_vtpm                 = true
    }
}
```
